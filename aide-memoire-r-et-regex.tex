%%% Copyright (C) 2024 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «Programmer avec R»
%%% https://gitlab.com/vigou3/programmer-avec-r
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[letterpaper,9pt,oneside,landscape,article,x11names]{memoir}
  \usepackage[english,french]{babel}
  \usepackage{enumitem}         % liste description
  \usepackage{pstricks}         % représentations 2D
  \usepackage{pst-solides3d}    % représentations 3D
  \usepackage{fontawesome5}     % flèches
  \usepackage{framed}           % env. framed

  %% Informations de publication
  \title{Aide-mémoire de programmation R et d'expressions régulières}
  \author{Vincent Goulet}

  %% Polices de caractères
  \usepackage{fontspec}
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale = 0.92}
  \setmainfont{Lucida Bright OT}[Ligatures = TeX]
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \usepackage[book,medium,proportional,lining]{FiraSans}
  \usepackage[babel=true]{microtype}
  \frenchsetup{og=«, fg=»}

  %% Style du document
  \setbeforesecskip{0ex}
  \setaftersecskip{1ex plus 0.1ex}
  \setsecheadstyle{\normalfont\sffamily\bfseries\centering}
  \setbeforesubsecskip{0.75ex plus 0.1ex}
  \setaftersubsecskip{0ex}
  \setsubsecheadstyle{\normalfont\bfseries\raggedright}
  \pagestyle{empty}
  \setlength{\parindent}{0pt}
  \setlength{\FrameSep}{2pt}     % env. framed

  %% Couleurs
  \usepackage{xcolor}
  \definecolor{url}{rgb}{0.6,0,0} % liens externes

  %% Hyperliens
  \usepackage{hyperref}
  \hypersetup{%
    pdfauthor = \theauthor,
    pdftitle = \thetitle,
    colorlinks = true,
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit}}

  %% Grille pour la disposition du matériel
  \usepackage[absolute]{textpos}
  \TPGrid[7.5mm,7.5mm]{12}{20}
  \TPMargin{0.025\TPHorizModule}

  %% Configuration de la liste 'description' utilisée pour les
  %% spécifications des fonctions
  \setlist[description]{%
    style=unboxed,
    leftmargin=2ex,
    topsep=0pt,
    format=\bfseries}

  %% Définition des représentations des matrices, tableaux et listes
  \psset{unit=1mm,linewidth=0.2}
  \setlength{\unitlength}{2\psunit}
  \input{include/matrice}
  \input{include/tableau}
  \input{include/liste}

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% La commande \meta de memoir requiert une déclaration \normalfont
  %% additionnelle pour utiliser la police par défaut dans le code.
  \makeatletter
  \def\meta@font@select{\normalfont\itshape}
  \makeatother

  %% Nouvelles commandes
  \newcommand{\code}[1]{\texttt{#1}}

\begin{document}

% \TPShowGrid{12}{20}

\raggedright

%%
%% Titre
%%
\begin{textblock}{12}[0.5,0.5](6,0)
  \centering
  \Large\sffamily\bfseries\thetitle
\end{textblock}

%%
%% Première colonne
%%

\begin{textblock}{3.9}(0,0.5)
  \vspace*{-\baselineskip}
  \begin{framed}
    \section*{Opérateurs (en ordre décroissant de priorité)}
    \begin{tabularx}{\linewidth}{@{}>{\bfseries}l>{\raggedright\arraybackslash}X@{}}
      \code{\$} & extraction d'une liste \\ %$
      \code{[}\, \code{[[} & indiçage \\
      \code{\string^} & puissance \\
      \code{-} & changement de signe \\
      {\NoAutoSpacing\verb|:|} & génération de suites \\
      \code{\%*\%}\, \code{\%\%}\, \code{\%/\%}\, \code{\%o\%} & produit matriciel,
                                                                 modulo,
                                                                 division entière,
                                                                 produit extérieur \\
      \code{*}\, \code{/} & multiplication, division \\
      \code{+}\, \code{-} & addition, soustraction \\
      \code{<}\, \code{<=}\, \code{==}\, \code{>=}\,
      \code{>}\, \code{!=} & plus petit,
                             plus petit ou égal,
                             égal,
                             plus grand ou égal,
                             plus grand,
                             différent de \\
      \code{!}  & négation logique \\
      \code{\&}\, \code{\&\&} & «et» logique \\
      \code{\textbar}\,
      \code{\textbar\textbar} & «ou» logique \\
      \code{->}\, \code{->>} & affectation \\
      \code{<-}\, \code{<<-} & affectation \\
    \end{tabularx}
  \end{framed}
\end{textblock}

\begin{textblock}{3.9}(0,7)
  \section*{Indiçage}
  \begin{description}
  \item[\code{x[\meta{entiers positifs}]}] éléments aux indices
    \meta{entiers positifs}
  \item[\code{x[\meta{entiers négatifs}]}] tous les éléments sauf ceux
    aux indices \meta{entiers négatifs}
  \item[\code{x[\meta{condition}]}] éléments satisfaisant la
    \meta{condition} (vecteur booléen)
  \item[\code{x[\meta{chaines}]}] éléments étiquetés \meta{chaines}
    \medskip
  \item[\code{x[i, ]}] lignes d'une matrice
  \item[\code{x[, j]}] colonnes d'une matrice
  \item[\code{x[i, , ]}] tranches horizontales d'un tableau
  \item[\code{x[, j, ]}] tranches verticales d'un tableau
  \item[\code{x[, , k]}] tranches transversales d'un tableau
  \item[\code{x[i, j, ]}] carottes horizontales d'un tableau
  \item[\code{x[i, , k]}] carottes transversales d'un tableau
  \item[\code{x[, j, k]}] carottes verticales d'un tableau
    \medskip
  \item[\code{x[[i]]}] élément \code{i} (unique) d'une liste
  \item[\code{x\$nom}] élément étiquetté \code{nom} (unique) d'une liste
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(0,15.5)
  \section*{Extraction}
  \begin{description}
  \item[\code{head(x, n)}] \code{n} premiers éléments de \code{x} (ou
    \code{x} sans les \code{n} derniers éléments si \code{n} $< 0$)
  \item[\code{tail(x, n)}] \code{n} derniers éléments de \code{x} (ou
    \code{x} sans les \code{n} premiers éléments si \code{n} $< 0$)
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(0,17.875)
  \section*{Tests logiques}
  \begin{description}
  \item[\code{is.na(x)}] \code{TRUE} pour les éléments \code{NA}
  \item[\code{all(x)}] \code{TRUE} si tous les éléments sont
    \code{TRUE}
  \item[\code{any(x)}] \code{TRUE} si au moins un élément est
    \code{TRUE}
  \end{description}
\end{textblock}


%%
%% Deuxième colonne
%%

\begin{textblock}{3.9}(4,0.5)
  \section*{Arrondi}
  \begin{description}
  \item[\code{round(x)}] arrondi
  \item[\code{floor(x)}] arrondi à l'entier inférieur
  \item[\code{ceiling(x)}] arrondi à l'entier supérieur
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(4,2.5)
  \section*{Sommaires pour les vecteurs}
  \begin{description}
  \item[\code{sum(x)}, \code{prod(x)}] somme et produit des éléments
    de \code{x}
  \item[\code{mean(x)}, \code{var(x)}] moyenne et variance des
    éléments de \code{x}
  \item[\code{min(x)}, \code{max(x)}] minimum et maximum de \code{x}
    \medskip
  \item[\code{diff(x)}] différences \code{x[2] - x[1]}, \code{x[3] -
      x[2]}, etc.
  \item[\code{cumsum(x)}, \code{cumprod(x)}] somme et produit cumulatif
  \item[\code{cummin(x)}, \code{cummax(x)}] minimum et maximum cumulatif
  \item[\code{pmin(x, y)}, \code{pmax(x, y)}] minimum et maximum
    élément par élément entre \code{x} et \code{y}
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(4,7)
  \section*{Sommaires pour les matrices}
  \begin{description}
  \item[\code{nrow(x)}, \code{ncol(x)}] nombre de lignes et de
    colonnes
  \item[\code{rowSums(x)}, \code{colSums(x)}] somme par ligne et par
    colonne
  \item[\code{rowMeans(x)}, \code{colMeans(x)}] moyenne par ligne et
    par colonne
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(4,9.375)
  \section*{Manipulation de chaines de caractères}
  \begin{description}
  \item[\code{paste(..., sep)}] chaines de caractères concaténées,
    séparées par défaut par des espaces
  \item[\code{paste0(...)}] chaines de caractères concaténées, sans
    séparateur
  \item[\code{nchar}] nombre de caractères dans une chaine
  \item[\code{substr(x, start, stop)}] sous-chaines allant du
    caractère en position \code{start} à celui en position \code{stop}
  \item[\code{strsplit(x, split)}] chaine \code{x} séparée en
    liste de sous-chaines selon le caractère \code{split}
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(4,13.75)
  \section*{Suites et répétition}
  \begin{description}
  \item[\code{seq(from, to, by, length.out, along.with)}] suites générales
  \item[\code{seq\_along(along.with)}] suite de $1$ à la longueur de
    \code{along.with}
  \item[\code{seq\_len(length.out)}] suite de $1$ à \code{length.out}
  \item[\code{rep(x, times, length.out, each)}] répétition d'un vecteur
  \item[\code{rep.int(x, times)}] répétition de \code{x} en entier
    \code{times} fois
  \item[\code{rep\_len(x, length.out)}] répétition de \code{x} jusqu'à
    un vecteur de longueur \code{length.out}
  \end{description}
\end{textblock}

%%
%% Troisième colonne
%%

\begin{textblock}{3.9}(8,0.5)
  \section*{Application pour les matrices et les tableaux}
  \begin{description}
  \item[\code{apply(X, MARGIN, FUN, ...)}] applique la fonction
    \code{FUN} sur les dimensions \code{MARGIN} de la matrice ou du
    tableau \code{X}
  \end{description}
  \begin{center}
    \begin{tabular}{*{4}{c@{}}c@{}}
      \code{X} & \multicolumn{3}{c}{\code{apply(X, 1, FUN)}} \\
      \begin{pspicture}(0,0)(12.5,11)
        \usebox{\pMatrix}
      \end{pspicture}
      & \raisebox{2\unitlength}{\quad\faArrowRight\quad} &
      \raisebox{2\unitlength}{\code{FUN} $\mapsto\;$}
      \begin{pspicture}(0,0)(12.5,11)
        \usebox{\aMatrixRow}
      \end{pspicture}
      & \raisebox{2\unitlength}{\quad\faArrowRight\quad} &
      \begin{pspicture}(0,0)(12.5,11)
        \usebox{\pVectorThree}
      \end{pspicture}
      \\[\unitlength]
      \code{X} & \multicolumn{3}{c}{\code{apply(X, 2, FUN)}} \\
      \begin{pspicture}(-5.5,-4.5)(8.5,7)
        \usebox{\pArray}
      \end{pspicture}
      & \raisebox{2.5\unitlength}{\quad\faArrowRight\quad} &
      \raisebox{2.5\unitlength}{\code{FUN} $\mapsto\;$}
      \begin{pspicture}(-6.5,-4.5)(11.0,7)
        \usebox{\aArraySliceV}
      \end{pspicture}
      & \raisebox{2.5\unitlength}{\quad\faArrowRight\quad} &
      \begin{pspicture}(0,-1)(12.5,7)
        \usebox{\pVectorFour}
      \end{pspicture}
    \end{tabular}
  \end{center}
\end{textblock}

\begin{textblock}{3.9}(8,5.75)
  \section*{Application pour les listes et les vecteurs}
  \begin{description}
  \item[\code{lapply(X, FUN, ...)}] applique la fonction \code{FUN} à
    chaque élément de la liste ou du vecteur \code{X} et retourne le
    résultat sous forme de \textbf{liste}
  \item[\code{sapply(X, FUN, ...)}] applique la fonction \code{FUN} à
    chaque élément de la liste ou du vecteur \code{X} et retourne le
    résultat sous forme de \textbf{vecteur}, si possible
  \end{description}
  \begin{center}
    \begin{tabular}{*{2}{c@{}}c}
      \multicolumn{3}{c}{\code{X}} \\
      \multicolumn{3}{c}{%
        \begin{pspicture}(0,0)(30,4.5)
          \usebox{\pList}
        \end{pspicture}} \\[\unitlength]
      \code{lapply(X, FUN)} \\
      \raisebox{0.75\unitlength}{\code{FUN} $\mapsto\;$}
      \begin{pspicture}(0,-1)(19,3.5)
        \rput(0,0){\usebox{\aListVectorThree}}
        \rput(9.5,0){\usebox{\aListVectorOne}}
        \rput(14,0){\usebox{\aListVectorTwo}}
      \end{pspicture}
      & \raisebox{0.75\unitlength}{\quad\faArrowRight\quad} &
      \begin{pspicture}(0,0)(30,3.5)
        \usebox{\pListOneOneOne}
      \end{pspicture} \\[\unitlength]
      \code{sapply(X, FUN)} \\
      \raisebox{0.25\unitlength}{\code{FUN} $\mapsto\;$}
      \begin{pspicture}(0,0)(19,3.5)
        \rput(0,0){\usebox{\aListVectorThree}}
        \rput(9.5,0){\usebox{\aListVectorOne}}
        \rput(14,0){\usebox{\aListVectorTwo}}
      \end{pspicture}
      & \raisebox{0.25\unitlength}{\quad\faArrowRight\quad} &
      \begin{pspicture}(0,0)(7.5,3.5)
        \usebox{\pListVectorThree}
      \end{pspicture}
    \end{tabular}
  \end{center}

  \begin{description}
  \item[\code{mapply(FUN, ...)}] applique \code{FUN} au vecteur des
    premiers éléments des arguments, au vecteur des deuxièmes
    éléments, etc.
  \end{description}
  \begin{center}
    \begin{tabular}{*{4}{c@{}}c}
      \multicolumn{5}{c}{\code{mapply(FUN, x, y)}} \\
      \begin{pspicture}(0,-4)(9,4.5)
        \rput(0,3.25){\code{x}}
        \rput(1.5,2){\usebox{\pListVectorThreeLight}}
        \rput(0,-1){\code{y}}
        \rput(1.5,-2){\usebox{\pListVectorThreeDark}}
      \end{pspicture}
      & \raisebox{2.25\unitlength}{\quad\faArrowRight\quad} &
      \raisebox{2.25\unitlength}{\code{FUN} $\mapsto\;$}
      \begin{pspicture}(0,-4)(5.5,7)
        \rput(0,4){\usebox{\aListVectorOneLight}}
        \rput(0,0){\usebox{\aListVectorOneLight}}
        \rput(0,-4){\usebox{\aListVectorOneLight}}
        \rput(3,4){\usebox{\aListVectorOneDark}}
        \rput(3,0){\usebox{\aListVectorOneDark}}
        \rput(3,-4){\usebox{\aListVectorOneDark}}
      \end{pspicture}
      & \raisebox{2.25\unitlength}{\quad\faArrowRight\quad} &
      \begin{pspicture}(0,-4)(9,4.5)
        \usebox{\pListVectorThreeLight}
      \end{pspicture}
    \end{tabular}
  \end{center}
\end{textblock}

\begin{textblock}{3.9}(8,15.25)
  \section*{Application pour les groupes de données}
  \begin{description}
  \item[\code{tapply(X, INDEX, FUN, ...)}] applique la fonction \code{FUN} à
    chacun des groupes du vecteur \code{X} définis par le facteur
    \code{INDEX}
  \end{description}
  \begin{center}
    \setlength{\unitlength}{2\psunit}
    \begin{tabular}{cc}
      \code{X} & \code{INDEX} \\
      \begin{pspicture}(0,0)(25,3)
        \usebox{\pListVectorTen}
      \end{pspicture}
      &
      \begin{pspicture}(0,0)(25,3)
        \usebox{\pListVectorIndex}
      \end{pspicture}
    \end{tabular}
    \\[\unitlength]
    \begin{tabular}{*{2}{c@{}}c}
      \code{tapply(X, INDEX, FUN)} \\
      \raisebox{2.25\unitlength}{\code{FUN} $\mapsto\;$}
      \begin{pspicture}(0,-4)(12.5,7)
        \rput(0,4){\usebox{\aListVectorThreeLight}}
        \rput(0,0){\usebox{\aListVectorFiveMed}}
        \rput(0,-4){\usebox{\aListVectorTwoDark}}
      \end{pspicture}
      & \raisebox{2.25\unitlength}{\quad\faArrowRight\quad} &
      \begin{pspicture}(0,-4)(12.5,2.5)
        \usebox{\pListVectorThreeLight}
      \end{pspicture}
    \end{tabular}
  \end{center}
\end{textblock}

\null\newpage

% \TPShowGrid{12}{20}

%%
%% Première colonne
%%

\begin{textblock}{3.9}(0,0)
  \section*{Produit extérieur}
  \begin{description}
  \item[\code{outer(X, Y, FUN)}] \code{FUN(X[i], Y[j])} pour toutes
    les combinaisons des éléments de \code{X} et \code{Y}
  \item[\code{x \%o\% y}] raccourci de \code{outer(x, y, "*")}
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(0,2)
  \section*{Tri et recherche}
  \begin{description}
  \item[\code{sort(x)}] \code{x} trié en ordre croissant (ou décroissant avec
    \code{decreasing = TRUE})
  \item[\code{rank(x)}] rangs des éléments de \code{x} dans l'ordre croissant
  \item[\code{order(x)}] ordre d'extraction des éléments pour trier
    \code{x} en ordre croissant (ou décroissant avec \code{decreasing
      = TRUE})
  \item[\code{rev(x)}] \code{x} en ordre inverse
  \item[\code{unique(x)}] éléments uniques de \code{x}
    \medskip
  \item[\code{match(x, table)}] indices des premières occurrences de
    \code{x} dans \code{table}
  \item[\code{x \%in\% table}] \code{TRUE} si \code{x} se trouve dans
    \code{table}
  \item[\code{which(x)}] indices des éléments \code{TRUE}
  \item[\code{which.min(x)}] indice du minimum
  \item[\code{which.max(x)}] indice du maximum
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(0,9)
  \section*{Structures de contrôle et boucles}
  \begin{description}
  \item[\code{if (\meta{cond}) \meta{expr} else \meta{alt}}] évaluer
    \meta{expr} si \meta{cond} est \code{TRUE} et \meta{alt} sinon
  \item[\code{ifelse(test, yes, no)}] vecteur formé des éléments de
    \code{yes} ou \code{no} selon que l'élément de \code{test} est
    \code{TRUE} ou \code{FALSE}
  \item[\code{return(value)}] retourner \code{value} et quitter la
    fonction
  \item[\code{stop(...)}] signaler une erreur et quitter la
    fonction
  \item[\code{warning(...)}] signaler un avertissement
    \medskip
  \item[for (\meta{variable} in \meta{suite}) \meta{expr}] boucle à
    dénombrement
  \item[while (\meta{cond}) \meta{expr}] boucle à précondition
  \item[repeat \meta{expr}] boucle à condition d'arrêt
  \item[\code{break}] force la sortie de la boucle
  \item[\code{next}] force le passage à la prochaine itération
  \item[\code{Recall(...)}] rappelle la fonction courante (récursion)
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(0,15.75)
  \section*{Bibliothèques et paquetages}
  \begin{description}
  \item[\code{library(package)}] charge \code{package} dans la session
    de travail
  \item[\code{install.packages(pkgs)}] installe \code{pkgs} à partir
    de CRAN dans la bibliothèque par défaut
  \item[\code{update.packages()}] met à jour les paquetages de toutes
    les bibliothèques
  \item[\code{.libPaths()}] liste des bibliothèques
  \end{description}
\end{textblock}

%%
%% Deuxième colonne
%%

\begin{textblock}{3.9}(4,0)
  \section*{Tests unitaires}
  \begin{description}
  \item[\code{source(file)}] évaluer \code{file} en entier
  \item[\code{stopifnot(...)}] arrêter si les expressions ne sont pas
    toutes \code{TRUE}
  \item[\code{identical(x, y)}] \code{TRUE} si les objets sont identiques
  \item[\code{all.equal(x, y)}] \code{TRUE} si \code{x} et \code{y}
    sont «presque égaux»
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(4,2.875)
  \section*{Importation et exportation}
  \begin{description}
  \item[\code{scan(file)}] données brutes du fichier \code{file} sous
    forme de vecteur simple
  \item[\code{read.table(file)}] données en tableau du
    fichier \code{file} sous forme de tableau de données
  \item[\code{read.csv(file)}] données en format CSV du fichier
    \code{file} sous forme de tableau de données
  \item[\code{readRDS(file)}] données sérialisées du fichier
    \code{file} sous forme d'objet R
    \medskip
  \item[\code{write(x, file, ncolumns)}] exporte le vecteur \code{x}
    sous forme de matrice de \code{ncolumns} colonnes
  \item[\code{write.table(x, file)}] exporte le tableau de données
    \code{x} sous forme de tableau
  \item[\code{write.csv(x, file)}] exporte le tableau de données
    \code{x} en format CSV
  \item[\code{saveRDS(object, file)}] exporte l'objet \code{object}
    sous forme de données sérialisées
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(4,10.375)
  \section*{Analyse et contrôle de texte}
  \begin{description}
  \item[\code{grep(pattern, x)}] indices des éléments de \code{x} qui
    correspondent au motif d'expression régulière \code{pattern}
  \item[\code{grepl(pattern, x)}] comme \code{grep} mais sous forme de
    vecteur booléen
  \item[\code{sub(pattern, replacement, x)}] remplace dans chaque
    élément de \code{x} la première correspondance du motif
    d'expression régulière \code{pattern} par \code{replacement}
  \item[\code{gsub(pattern, replacement, x)}] comme \code{sub} mais
    pour toutes les correspondances
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(4,14.75)
  \vspace*{-\baselineskip}
  \begin{framed}
    \section*{Expressions régulières: opérateurs de base}
    \begin{tabularx}{\linewidth}{@{}>{\bfseries}l>{\raggedright\arraybackslash}X}
      \code{\textbar} & alternance («ou») \\
      \code{\string^} & début de la ligne;
                        négation (à l'intérieur d'une classe) \\
      \code{\$} & fin de la ligne \\
      \code{.} & caractère de remplacement \\
      \code{*} & zéro, une ou plusieurs occurrences \\
      \code{+} & une ou plusieurs occurrences \\
      \code{?} & zéro ou une occurrence \\
      \code{(~)} & groupe de caractères \\
      \code{[~]} & classe de caractères \\
      \code{\{~\}} & quantificateur du nombre d'occurrences \\
      \code{\bs} & caractère d'échappement \\
    \end{tabularx}
  \end{framed}
\end{textblock}

%%
%% Troisième colonne
%%

\begin{textblock}{3.9}(8,0)
  \section*{Expressions régulières: outils}

  Dans les définitions de syntaxe ci-dessous, les éléments entre
  crochets \code{[~]} sont optionnels.

  \begin{description}[style=nextline,topsep=3pt]
  \item[\code{grep [-Ecov] '\meta{motif}' \meta{fichiers}}] %
    affiche les lignes de \meta{fichiers} qui correspondent à
    \meta{motif}
  \begin{description}
  \item[\code{-E}] motif est une expression régulière étendue
  \item[\code{-c}] nombre de lignes qui correspondent au motif
  \item[\code{-o}] portion de la ligne qui correspond au motif
  \item[\code{-v}] lignes qui ne correspondent \emph{pas} au motif
  \end{description}
  \end{description}

  \begin{description}[style=nextline,topsep=3pt]
  \item[\code{sed [-E] 's/\meta{motif}/\meta{remplacement}/[g]'
      \meta{fichiers}}] %
    remplace dans chaque ligne de \meta{fichiers}
    la première occurrence qui correspond à \meta{motif} par
    \meta{remplacement}
    \begin{description}
    \item[\code{-E}] motif est une expression régulière étendue
    \item[\code{g}] remplace toutes les occurrences sur une ligne
    \end{description}
  \end{description}

  \begin{description}[style=nextline,topsep=3pt]
  \item[\code{awk '\meta{motif} \{ \meta{action} \} ...' \meta{fichiers}}] %
    sépare chaque enregistrement de \meta{fichiers} en champs puis
    exécute tour à tour \meta{action} (\code{print} par défaut)
    quand un enregistrement correspond à \meta{motif}

    \subsection*{Variables automatiques}
    \begin{description}
    \item[\code{\$0}, \code{\$1}, \code{\$2}, \dots] contenu de
      l'enregistrement complet, du premier champ, du deuxième champ,
      \dots
    \item[\code{NR}] numéro de l'enregistrement courant
    \item[\code{NF}] nombre de champs dans l'enregistrement courant
    \item[\code{FS}] séparateur de champs lors de la lecture
    \item[\code{OFS}] séparateur de champs lors de l'écriture
    \end{description}
    \subsection*{Motifs}
    \begin{description}
    \item[\code{/\meta{regex}/}] correspond lorsque \meta{regex}
      correspond
    \item[\code{\meta{expression}}] correspond lorsque
      \meta{expression} est non nulle ou non vide
    \item[\code{BEGIN}] correspond avant la lecture du premier enr.
    \item[\code{END}] correspond après la lecture du dernier enr.
    \end{description}
    \subsection*{Opérateurs et fonctions}
    \begin{description}
    \item[\code{print \meta{chaine}}] affiche \meta{chaine}
      (\code{\$0} par défaut)
    \item[\code{\meta{chaine} \string~ /\meta{regex}/}] vrai lorsque
      \meta{chaine} correspond à \meta{regex}
    \item[\code{\meta{chaine} !\string~ /\meta{regex}/}] vrai lorsque
      \meta{chaine} ne correspond pas à \meta{regex}
    \item[\code{\meta{cond} ? \meta{expr} : \meta{alt}}] exécute
      \meta{expr} si \meta{cond} est vraie et \meta{alt} sinon
    \end{description}
  \end{description}
\end{textblock}

\begin{textblock}{3.9}(8,16.125)
  \section*{Ligne de commande Unix}
  \begin{description}
  \item[\code{\meta{commande~1} | \meta{commande~2}} (transfert de
    données ou \emph{tuyau})] envoyer la sortie de \meta{commande~1}
    en entrée à \meta{commande~2}
  \item[\code{\meta{commande} > \meta{fichier}} (redirection)] envoyer
    la sortie de \meta{commande} dans \meta{fichier}
  \item[\code{\meta{commande} < \meta{fichier}} (redirection)] envoyer
    le contenu de \meta{fichier} en entrée à \meta{commande}
  \end{description}
\end{textblock}

%%
%% Notice de copyright
%%
\begin{textblock}{12}[0.5,0.5](6,20)
  \small\centering
  {\footnotesize\ccbysa} {\theauthor}. Ce document est mis à
  disposition sous licence
  \href{http://creativecommons.org/licenses/by-sa/4.0/deed.fr}{%
    Attribution-Partage dans les mêmes conditions 4.0 International}
  de Creative Commons.
\end{textblock}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
